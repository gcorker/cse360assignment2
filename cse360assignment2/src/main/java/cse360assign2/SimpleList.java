/*Author: Graham Corker
 * Class ID: 350
 * Assignment 2
 * Bitbucket Link: https://bitbucket.org/gcorker/cse360assignment2/src/master/
 * This file contains a class called SimpleList that can be used to modify the contents of a list that is created
 * 		upon instantiation.  The operations SimpleList can perform are "add", "remove", "count", "toString", "search",
 * 		"append", "first", "last", and "size"
 * */

package cse360assign2;

// this class represents an object that has a list and a count
// operations can be performed on the list in order to modify its contents
public class SimpleList {
	private int[] list;
	private int count;
	
	// constructor creates list of size 10 and initializes count to 0
	// takes in no parameters and is a void
	public SimpleList() {
		this.list = new int[10];
		this.count = 0;
	}
	
	// increases the length of this.list by creating an array 50% larger than list
	// takes in no parameters and is void
	private void lengthen() {
		if(count == list.length) {
			int[] newList = new int[list.length + list.length/2];
		
			for(int index = 0; index < list.length; index++) {
				newList[index] = list[index];
			}
			list = newList;
		}
	}
	
	// shortens the length of this.list if there are enough free spaces
	// takes in no parameters and is void
	private void shorten() {
		int quarterSize = list.length/4;
		int emptySpace = list.length - count;
		
		if(emptySpace >= quarterSize && list.length - quarterSize >= 1) {
			int[] newList = new int[list.length - quarterSize];
			
			for(int index = 0; index < newList.length; index++) {
				newList[index] = list[index];
			}
			list = newList;
		}
	}
	
	// adds a number to index 0 of this.list and shifts other numbers over
	// takes in an int parameter that will be inserted into the list and returns nothing
	public void add(int number) {
		lengthen();
		
		// iterates backwards through the list, passing values up until the first index
		// can have a value passed into it without removing other values
		for(int index = count - 1; index >= 0; index --) {
			
			if(index != list.length - 1) {
				list[index + 1] = list[index];
			}
		}
		
		list[0] = number;
		
		if(count < list.length) {
			count ++;
		}
	}
	
	// removes number from this.list and shifts other numbers back into place
	// takes in an int parameter that will be removed from the list and returns nothing
	public void remove(int number) {
		int numberIndex = search(number);
		
		if(numberIndex != -1) {
			for(int index = numberIndex; index < count - 1; index ++) {
				list[index] = list[index + 1];
			}
			
			count --;
		}
		shorten();
	}
	
	// returns the value of this.count
	// takes in no parameters and returns this.count
	public int count() {
		return count;
	}
	
	//prints this.list as a string, with elements separated by spaces
	// takes in no parameters and returns the data in this.list represented as a string
	public String toString() {
		String returnString = new String("");
		
		for(int index = 0; index < count; index ++){
			returnString += list[index];
			
			if(index != count - 1) {
				returnString += " ";
			}
		}
		
		return returnString;
	}
	
	// searches for a number in this.list 
	// takes in one parameter which is the number that will be searched for
	// returns the index of the parameter desired and if it is not found, returns -1
	public int search(int number) {
		int foundIndex = -1;
		
		for(int index = 0; index < count; index ++) {
			if(list[index] == number && foundIndex == -1) {
				foundIndex = index;
			}
		}
		
		return foundIndex;
	}
	

	// appends a number to the end of this.list
	// takes in a parameter that is the value to be added to the end
	// returns nothing
	public void append(int parameter) {
		lengthen();
		
		list[count] = parameter;
		
		count ++;
	}
	
	// this finds the first element in this.list
	// the method takes in no parameters
	// returns the first element in this.list if there are elements in the list.  Returns -1 otherwise
	public int first() {
		int returnVal = -1;
		
		if(count > 0) {
			returnVal = list[0];
		}
		return returnVal;
	}
	
	// finds the last element in this.list
	// the method takes in no parameters
	// returns the last element in this.list if there are elements in the list.  Returns -1 otherwise
	public int last() {
		int returnVal = -1;
		
		if(count > 0) {
			returnVal = list[count - 1];
		}
		return returnVal;
	}
	
	// this method provides the current number of possible locations in this.list
	// the method takes in no parameters
	// returns the current number of possible locations in this.list
	public int size() {
		return list.length;
	}

	
}

